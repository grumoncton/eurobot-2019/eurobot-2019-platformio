filename="/tmp/platformio_firmware_versions/$1"
mkdir -p `dirname $filename`
fw_version=$(date +%s | tee $filename)
echo -n "-D FW_VERSION=$fw_version"
