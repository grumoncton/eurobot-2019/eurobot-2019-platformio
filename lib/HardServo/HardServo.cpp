#include "HardServo.h"

HardServo::HardServo(PinName pin) : _pwm(pin) {
	_pwm.period(0.02);
}

void HardServo::SetPosition(float pulse) {
    _pwm.pulsewidth(pulse/1e6);
}
