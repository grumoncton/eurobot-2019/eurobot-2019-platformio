#ifndef HARD_SERVO_H
#define HARD_SERVO_H

#include "mbed.h"

class HardServo {

public:

    HardServo(PinName pin);
    void SetPosition(float pulse);

protected:
    PwmOut _pwm;
};

#endif