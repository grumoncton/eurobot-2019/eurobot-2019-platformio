#ifndef BOOTLOADER_H
#define BOOTLOADER_H

#include <stdint.h>
#include "stm32-constants.h"

// Address of System Memory (ST Bootloader)
#ifdef STM32F401xE
#define SYSMEM_ADDRESS (uint32_t)0x1FFF0000
#endif
#ifdef STM32F303x8
#define SYSMEM_ADDRESS (uint32_t)0x1FFFD800
#endif
#ifdef TEST
#define SYSMEM_ADDRESS (uint32_t)0
#endif

#undef UNUSED
#define UNUSED(x) ((void)((uint32_t)(x)))

typedef void (*pFunction)(void);

void jumpToBootloader();

#endif
