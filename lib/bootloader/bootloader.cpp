#include "bootloader.h"

void jumpToBootloader() {
  uint32_t jumpAddress = *(__IO uint32_t*)(SYSMEM_ADDRESS + 4);
  pFunction jump = (pFunction)jumpAddress;

  HAL_RCC_DeInit();
  HAL_DeInit();

  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;

  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();

  __set_MSP(*(__IO uint32_t*)SYSMEM_ADDRESS);
  jump();

  while(1);
}
