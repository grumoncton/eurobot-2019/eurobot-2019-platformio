#include "async-servo.h"
#include "mbed.h"

AsyncServo::AsyncServo(PinName Pin) : ServoPin(Pin) {}

void AsyncServo::SetPosition(int Pos) {
	currentPWM = Pos;
	target = Pos;
}

void AsyncServo::StartPulse() {
	ServoPin = 1;

	if (currentPWM < target - currentDelta) {
		currentPWM += currentDelta;
	} else if (currentPWM > target + currentDelta) {
		currentPWM -= currentDelta;
	}

	timeout.attach_us(callback(this, &AsyncServo::EndPulse), currentPWM);
}

void AsyncServo::EndPulse() {
	ServoPin = 0;
}

void AsyncServo::Enable(int StartPos, int target) {
	this->currentPWM = target;
	this->target = target;
	ticker.attach_us(callback(this, &AsyncServo::StartPulse), currentPWM);
}

void AsyncServo::Disable() {
	ticker.detach();
}

void AsyncServo::startSlowRotation(int target, int steps) {
	this->target = target;
	this->currentDelta = abs(currentPWM - target) / steps;
	printf("Current delta %d current pwm %d\n", this->currentDelta, this->currentPWM);
}
