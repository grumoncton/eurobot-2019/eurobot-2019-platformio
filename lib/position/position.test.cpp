#ifdef TEST

#include "position.test.h"

Test(position, instantiate_position) {
	cr_expect(pos.x == 0 && pos.y == 0 && pos.a == 0,
			"Expect initial values to be (0, 0, 0)");
}

#endif
