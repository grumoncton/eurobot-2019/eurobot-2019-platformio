#ifndef POSITION_H
#define POSITION_H

#include <math.h>

typedef struct position {
	float x;
	float y;
	// Angle is in radians and is always between 0 and 2 pi
	float a;
} position_t;

extern position_t pos;

#endif
