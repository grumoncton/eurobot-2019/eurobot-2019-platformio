#include "comms.h"


uint8_t successiveBadReads = 0;


/**
 * Prepare frame
 *
 * Clear data, write given args to beginning, add crc.
 */

void prepareFrame(const char *args, const size_t length) {

	// Clear any existing data
	memset(comms.responseFrame, 0, ARG_LENGTH);

	// Copy given args to frame
	memcpy(comms.responseFrame, args, length);

	// Calculate frame's checksum
	uint16_t crc = crc16(comms.responseFrame, ARG_LENGTH);

	// Add crc to frame
	for (uint8_t i = 0; i < CRC_LENGTH; i++) {
		comms.responseFrame[ARG_LENGTH + i] = (crc >> (8 * i)) & 0xFF;
	}
}

void prepareFrame(const char *args) {
	prepareFrame(args, strlen(args));
}

void prepareFrame(const char arg) {
	const char args[] = { arg };
	prepareFrame(args, 1);
}


/**
 * Handle fully assembled command
 */

void commsHandleCommand() {

	if (readInstructionAndFrame(comms.instruction, comms.frame) != 0) {
#ifdef DEBUG
		printf("Error reading frame.\n");
#endif

		successiveBadReads++;

		if (successiveBadReads > MAX_SUCCESSIVE_BAD_READS) {
			i2c.stop();
			wait_us(100);
			i2c.address(comms.addr << 1);

			successiveBadReads = 0;
		}

		return;
	}

	successiveBadReads = 0;

	if (comms.instruction == (char) BaseInstruction::GET_RESPONSE) return;

	wait_us(5e3);

	memcpy(comms.args, comms.frame, ARG_LENGTH);

#ifdef DEBUG
	printf("Instruction %02x args: ", comms.instruction);
	for (int i = 0; i < ARG_LENGTH; i++) {
		if (i > 0) printf(":");
		printf("%02X", comms.args[i]);
	}
	printf("\n");
#endif

	// Extract crc from frame
	uint16_t crc = 0;
	for (uint8_t i = 0; i < CRC_LENGTH; i++) {
		// crc is little endian in message
		crc += comms.frame[ARG_LENGTH + i] << (8 * i);
	}

#ifdef DEBUG
	printf("Received checksum: %d\n", crc);
#endif

	// Calculate crc and compare
	if (crc != crc16(comms.instruction, comms.args)) {

#ifdef DEBUG
		printf("Bad checksum\n");
#endif

		commsNack();
		return;
	}

	// Check for matching command
	instruction_handler_t *handler = comms.instruction_handlers;

	while (handler != NULL) {

#ifdef DEBUG
		printf("Checking handler 0x%x\n", handler->instruction);
#endif

		if (handler->instruction == comms.instruction) {
#ifdef DEBUG
		printf("Found handler 0x%x\n", handler->instruction);
#endif
			handler->callback();
			return;
		}

		handler = handler->next;
	}

	prepareFrame((char) BaseInstruction::NO_HANDLER_FOUND);
}


/**
 * Initialize communications
 */

void commsInit(uint8_t addr) {
	i2c.address(addr << 1);

	comms.addr = addr;

	initBaseInstructions();

	// Serial ser(USBTX, USBRX);
#ifdef DEBUG
	printf("Initialized\n");
#endif
}


/**
 * Comms main loop
 */

void commsLoop() {
	switch (i2c.receive()) {
		case I2CSlave::ReadAddressed: {
			if (comms.instruction == (char) BaseInstruction::GET_RESPONSE) {
				i2c.write(comms.responseFrame, FRAME_LENGTH);
			} else {
				i2c.write(0x00);
			}
			break;
		}
		case I2CSlave::WriteAddressed: {
			commsHandleCommand();
			break;
		}
	}
}
