#ifndef COMMS_CONSTANTS_H
#define COMMS_CONSTANTS_H

#include <stdint.h>
#include <stm32f3xx.h>
#include <mbed.h>
#include "crc.h"
#include "comms-status.h"
#include "comms-instruction-handler.h"


#define FRAME_LENGTH (31)
#define ARG_LENGTH ((FRAME_LENGTH) - (CRC_LENGTH))

// If a response isn't received in `COMMS_RES_TIMEOUT` seconds, the last message
// is re-sent
#define COMMS_RES_TIMEOUT (200e-3)

struct Comms {
	uint8_t addr;
	char instruction;
	char frame[FRAME_LENGTH];
	char args[ARG_LENGTH];
	char responseFrame[FRAME_LENGTH];
	CommsStatus status;

	instruction_handler_t *instruction_handlers;
};

extern Comms comms;

#endif
