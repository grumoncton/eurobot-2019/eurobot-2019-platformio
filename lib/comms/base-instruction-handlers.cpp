#include "base-instruction-handlers.h"

void commsAck() {
	prepareFrame((const char) BaseInstruction::ACK);
}

void commsNack() {
	prepareFrame((const char) BaseInstruction::NACK);
}

void commsJumpToBootloader() {
	commsAck();
	wait(1);
	jumpToBootloader();
}

void commsSendInfo() {
	char args[ARG_LENGTH] = { };
	sprintf(args, "%s.%u", BOARD_NAME, FW_VERSION);
	prepareFrame(args);
}



void commsGetStatus() {
	prepareFrame((const char) comms.status);
}

void initBaseInstructions() {
	addInstructionHandler((char) BaseInstruction::ENTER_BOOTLOADER, commsJumpToBootloader);
	addInstructionHandler((char) BaseInstruction::GET_INFO, commsSendInfo);
	addInstructionHandler((char) BaseInstruction::ACK, commsAck);
	addInstructionHandler((char) BaseInstruction::NACK, commsNack);
	// addInstructionHandler((char) BaseInstruction::SET_TABLE_SIDE, handleSetTableSide);
	addInstructionHandler((char) BaseInstruction::GET_STATUS, commsGetStatus);
}
