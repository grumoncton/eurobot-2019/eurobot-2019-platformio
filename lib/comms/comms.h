#ifndef COMMS_H
#define COMMS_H

#include <stdint.h>
#include <mbed.h>
#include "crc.h"
#include "i2c.h"
#include "bootloader.h"
#include "comms-instruction-handler.h"
#include "base-instructions.h"
#include "base-instruction-handlers.h"
// #include "env-instructions.h"
// #include "env-instruction-handlers.h"
#include "comms-constants.h"
// #include "comms-commands.h"

const uint8_t MAX_SUCCESSIVE_BAD_READS = 2;

void commsInterruptHandler();
void commsResetMessage();
void prepareFrame(const char *args, const size_t length);
void prepareFrame(const char *args);
void prepareFrame(const char arg);
void commsHandleCommand();
void commsInit(uint8_t addr);
void commsLoop();

#endif
