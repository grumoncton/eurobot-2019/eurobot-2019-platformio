#include "i2c.h"

bool isFirstRead = true;
uint8_t returnValue;

I2CSlave i2c(PB_7, PB_6);

uint8_t readInstructionAndFrame(char &instruction, char *frame) {
	if (isFirstRead) {
		isFirstRead = false;

		char data[FRAME_LENGTH + 1];
		returnValue = i2c.read(data, sizeof(data));

		if (returnValue != 0) return returnValue;

		instruction = data[0];

		memcpy(frame, data + 1, FRAME_LENGTH);

		return returnValue;
	} else {
		returnValue = i2c.read(&instruction, 1);

		if (returnValue != 0) return returnValue;

		return i2c.read(frame, FRAME_LENGTH);
	}
}
