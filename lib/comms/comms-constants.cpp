#include "comms-constants.h"

Comms comms = {
	.addr = 0,
	.instruction = 0,
	.frame = {},
	.args = {},
	.responseFrame = {},
	.status = CommsStatus::INITIALISING,

	.instruction_handlers = NULL,
};
