#ifndef I2C_H
#define I2C_H

#include <mbed.h>
#include "comms-constants.h"

extern I2CSlave i2c;

uint8_t readInstructionAndFrame(char &instruction, char *args);

#endif
