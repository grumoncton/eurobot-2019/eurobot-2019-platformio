#include "crc.h"

uint16_t crc16(const char *ptr, int count) {
	uint16_t crc;
	uint8_t i;
	crc = 0;
	while (--count >= 0) {
		crc = crc ^ (int) *ptr++ << 8;
		i = 8;
		do {
			if (crc & 0x8000) {
				crc = crc << 1 ^ 0x1021;
			} else {
				crc = crc << 1;
			}
		} while(--i);
	}
	return crc;
}

uint16_t crc16(const char instruction, const char *args) {
	char data[ARG_LENGTH + 1];
	data[0] = instruction;
	memcpy(data + 1, args, ARG_LENGTH);

	uint16_t crc = crc16((const char *) data, sizeof(data));

#ifdef DEBUG
	printf("Calculated CRC: %d\n", crc);
#endif

	return crc;
}
