// #include "comms-commands.test.h"

// void mockedSendMessage(char *message) {}

// Test(comms_commands, ack) {

// 	// Setup mocks
// 	Fake(Method(MockedLowPowerTimer, stop));
// 	LowPowerTimer &responseTimer = MockedLowPowerTimer.get();
// 	comms.responseTimer = &responseTimer;

// 	// Set intial state
// 	comms.isWaitingForAck = true;

// 	// Ensure initial state as we expect
// 	cr_expect(comms.isWaitingForAck == true,
// 			"Initially set `isWaitingForAck` to check if set");
// 	Verify(Method(MockedLowPowerTimer, stop)).Exactly(0_Times);

// 	// Run test function
// 	commsAck(mockedSendMessage);

// 	// Ensure side effects we expect
// 	cr_expect(comms.isWaitingForAck == false,
// 			"It should set `isWaitingForAck` to false");
// 	Verify(Method(MockedLowPowerTimer, stop)).Exactly(Once);
// }
