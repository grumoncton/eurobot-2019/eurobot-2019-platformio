#ifdef TEST

#include <criterion/criterion.h>
#include "crc.test.h"

const char message[] = "Hello world!";

Test(crc, calculate_chcksum) {
	cr_expect(crc16((char *) message, strlen(message)) == 14811,
			"Expect checksum to be 14811");
}

#endif
