#ifdef TEST

#include "comms.test.h"

Test(comms, commesInterruptHandler) {

	// Setup mocks
	Fake(Method(MockedLowPowerTimer, reset));
	Fake(Method(MockedLowPowerTimer, start));
	LowPowerTimer &characterTimer = MockedLowPowerTimer.get();
	comms.responseTimer = &characterTimer;
	comms.characterTimer = &characterTimer;

	// Character timeout should initially not be called
	Verify(Method(MockedLowPowerTimer, reset)).Exactly(0_Times);
	Verify(Method(MockedLowPowerTimer, start)).Exactly(0_Times);

	// It should build string
	cr_expect(strcmp((const char *) comms.message, "") == 0,
			"Message should initially be empty");
	cr_expect_eq(comms.isCommandWaiting, false,
			"It should initially not have commands waiting");

	const char message[] = "Test string";
	ser._getcSetReturnValues(message);
	for (uint8_t i = 0; i < strlen(message); i++) {
		commsInterruptHandler();
	}

	cr_expect_eq(comms.isCommandWaiting, false,
			"It should still not have command waiting before newline");

	ser._getcSetReturnValues("\n");
	commsInterruptHandler();

	cr_expect_eq(comms.isCommandWaiting, true,
			"Once it receives a newline, it should have command waiting");
	cr_expect_eq(comms.message[strlen((const char *) comms.message)], '\0',
			"It should add null terminator to message");

	// It should call timer methods
	Verify(Method(MockedLowPowerTimer, reset)).Exactly(strlen(message));
	Verify(Method(MockedLowPowerTimer, start)).Exactly(strlen(message));

	cr_expect_eq(strlen((const char *) comms.message), strlen(message),
			"Message should now have been constructed");
	cr_expect_str_eq((const char *) comms.message, message,
			"Message should now have been constructed");
}

Test(comms, commsResetMessage) {

	// Set initial values
	const char initialMessage[] = "Some initial message";
	strncpy((char *) comms.message, initialMessage, strlen(initialMessage));
	comms.messagePosition = strlen(initialMessage);
	comms.isCommandWaiting = true;

	// Check initial values
	cr_expect_str_eq((const char *) comms.message, initialMessage,
			"It should initially have initial message");
	cr_expect_eq(comms.messagePosition, strlen(initialMessage),
			"It should initially have initial message position");
	cr_expect_eq(comms.isCommandWaiting, true,
			"It should initially have message waiting");

	commsResetMessage();

	cr_expect_str_eq((const char *) comms.message, "",
			"It should have reset message");
	cr_expect_eq(comms.messagePosition, 0,
			"It should have reset message position");
	cr_expect_eq(comms.isCommandWaiting, false,
			"It should now not have message waiting");
}

#endif
