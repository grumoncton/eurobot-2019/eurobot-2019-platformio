#ifndef COMMS_COMMANDS_H
#define COMMS_COMMANDS_H

#include <mbed.h>
#include "comms.h"
#include "bootloader.h"
#include "comms-constants.h"
#include "base-instructions.h"
#include "robot-side.h"

#ifndef BOARD_NAME
#define BOARD_NAME "test_board"
#endif

#ifndef FW_VERSION
#define FW_VERSION 0
#endif

void commsAck();
void commsNack();
void commsJumpToBootloader();
void commsSendInfo();

void initBaseInstructions();

#endif
