#include <stdint.h>
#include "comms-constants.h"

#define CRC_LENGTH (2)

uint16_t crc16(const char *ptr, int count);
uint16_t crc16(const char instruction, const char *args);
