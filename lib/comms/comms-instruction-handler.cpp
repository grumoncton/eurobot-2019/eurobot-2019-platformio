#include "comms-instruction-handler.h"
#include "comms-constants.h"

void addInstructionHandler(char instruction, void (*callback)()) {
	instruction_handler_t *handler = new instruction_handler;

	handler->instruction = instruction;
	handler->callback = callback;
	handler->next = NULL;

	if (comms.instruction_handlers == NULL) {
		comms.instruction_handlers = handler;
	} else {
		instruction_handler_t *tail = comms.instruction_handlers;

		while (tail->next != NULL) {
			tail = tail->next;
		}

		tail->next = handler;
	}
}

void addInstructionHandler(char instruction, uint8_t (*callback)()) {
	addInstructionHandler(instruction, callback);
}
