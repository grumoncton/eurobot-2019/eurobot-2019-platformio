#ifndef COMMS_STATUS_H
#define COMMS_STATUS_H

enum class CommsStatus {
	INITIALISING = 0,
	READY = 1,
	WORKING = 2,
};

#endif
