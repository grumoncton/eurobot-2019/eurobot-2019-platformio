#ifndef COMMS_INSTRUCTION_HANDLER
#define COMMS_INSTRUCTION_HANDLER

#include <cstddef>
#include <stdlib.h>
#include <stdint.h>

// Struct for instruction that the pi can call on us
// Command is and instruction (single byte) plus arguments
typedef struct instruction_handler {
  char instruction;
  void (*callback)();
  struct instruction_handler *next;
} instruction_handler_t;

void addInstructionHandler(char instruction, void (*callback)());
void addInstructionHandler(char instruction, uint8_t (*callback)());

#endif
