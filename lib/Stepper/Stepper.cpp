/* mbed Stepper Library
 * Copyright (c) 2010 fachatz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "Stepper.h"
#include "mbed.h"

// define Library version number
#define VERSION 0.3

#define min(a, b) (a > b ? b : a)
#define max(a, b) (a < b ? b : a)

// led4, used for testing the direction signal
DigitalOut led4(LED4);

Stepper::Stepper(PinName clkPin, PinName dirPin, uint16_t startStopDelay, PinName enablePin):
		_clk(clkPin),
		_dir(dirPin),
		_en(enablePin) {

	this->enablePin = enablePin;
	this->clkPin = clkPin;
	this->dirPin = dirPin;

	this->startStopDelay = startStopDelay;
	this->done = true;

	_clk = false;
	_dir = false;

	if (enablePin != NC) {
		_en = true;
	}
}

void Stepper::setTarget(uint32_t target, bool direction, uint16_t delay, bool accel) {
	startRotation(direction, delay, accel);
	this->target = target;
	this->infinite = false;
}

void Stepper::startRotation(bool direction, uint16_t delay, bool accel) {
	this->currentStep = 0;
	this->infinite = true;
	this->direction = direction;
	this->targetDelay = delay;
	this->accel = accel;
	this->currentDelay = accel ? startStopDelay : delay;
	this->done = false;


	enable();

	// update();
	this->timeout.attach_us(callback(this, &Stepper::update), this->currentDelay);
}

void Stepper::reset() {
	this->currentStep = 0;
	this->target = 0;
	this->done = true;

	this->disable();

	this->timeout.detach();
}

void Stepper::update() {

	// If we've gone far enough, return early
	if (!this->infinite && this->currentStep >= this->target) {
		reset();

		return;
	}

	if (this->accel) {

		// Ramp up
		if (this->currentStep < this->startStopDelay) {
			this->currentDelay = max(this->currentDelay - 1, this->targetDelay);
		}

		// Ramp down
		if (this->currentStep > this->target - this->startStopDelay) {
			this->currentDelay = max(this->currentDelay + 1, this->startStopDelay);
		}
	}

	this->timeout.attach_us(callback(this, &Stepper::update), this->currentDelay);

	_dir = led4 = direction;

	// Pulse clk for 1us
	_clk = 1;
	wait_us(1);
	_clk = 0;

	this->currentStep++;
}

void Stepper::enable() {
	if (this->enablePin != NC) {
		_en = false;
	}
}

void Stepper::disable() {
	if (this->enablePin != NC) {
		_en = true;
	}
}

// version() returns the version number of the library
float Stepper::version(void) {
	return VERSION;
}
