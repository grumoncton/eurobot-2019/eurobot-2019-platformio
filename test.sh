#!/usr/bin/zsh

set -e

compile() {
	g++ -Wall -O0 \
		-DTEST \
		-flto \
		-fprofile-arcs -ftest-coverage \
		-I$root/.piolibdeps/FakeIt/include -I$root/.piolibdeps/FakeIt/config/standalone \
		-I$root/mocks -I$root/include $(ls $root/lib/* -1d | xargs printf ' -I%s ')\
		-c "$1" \
		-o "$2"
}


root=$(cd `dirname $0` && pwd)
orig_pwd=$(pwd)

test_dir="$root/.tests"
test_cpp="$test_dir/test.cpp"
test_exe="$test_dir/test"

mkdir -p "$test_dir"
cd "$test_dir"

source_cpp_files=$(find $root -type f -name '*.cpp' -not -path '*/.piolibdeps/*' -not -path '*/.pioenvs/*' -not -path "$test_dir/*")

echo -e "\n>>> Generating \`test.cpp\`...\n"

echo "" > "$test_cpp"
for f in $(find "$root" -type f -name '*.test.h'); do
	echo "#include \"$(basename $f)\"" >> $test_cpp
done

for f in $(<<< $source_cpp_files); do
	out="$(dirname ${f/$root/$test_dir})/$(basename ${f%.*}).o"

	[ -e "$out" ] && [ $(date +%s -r "$out") -gt $(date +%s -r "$f") ] && continue

	echo ">>> Compiling ${out/$test_dir\//}"
	mkdir -p $(dirname $out)

	compile $f $out
done

echo ">>> Compiling $test_exe.o"
compile "$test_cpp" "$test_exe.o"

echo -e "\n>>> Linking \`test.o\`...\n"

g++ -Wall -O0 \
	-flto \
	-fprofile-arcs -ftest-coverage \
	-lm -lcriterion -lgcov \
	-I$root/.piolibdeps/FakeIt/include -I$root/.piolibdeps/FakeIt/config/standalone \
	-I$root/mocks -I$root/include $(ls $root/lib/* -1d | xargs printf ' -I%s ')\
	-o "$test_exe" $(find $test_dir -name '*.o')
	# || tput setaf 1 && echo -e "\n>>> Errors occured. Compilation terminated.\n" && exit 1

echo -e "\n>>> Executing test file...\n"

"$test_exe"

echo -e "\n>>> Calculating coverage...\n"

for f in $(<<< $source_cpp_files); do
	file_dirname="$(dirname ${f/$root/$test_dir})"
	file_basename="$file_dirname/$(basename ${f%.*})"

	if [ -e "$file_basename.gcda" ]; then
		# If gcda file exists, extract coverage from it
		coverage="$(gcov --no-output $f -o $file_dirname | sed -n 2p | sed 's/Lines executed:\([^%]\+\).*/\1/')"
	else
		# If gcno file exists but no gcda, file has no functions but was still
		# executed
		[ -e "$file_basename.gcno" ] && coverage="100.00" || coverage="0.00"
	fi

	# If the file has not executable lines, it was executed
	if [ "$coverage" = "No executable lines" ]; then
		coverage="100.00"
	fi

	if (( $(echo "$coverage > 80" | bc -l) )); then
		colour="$(tput setaf 2)"
	elif (( $(echo "$coverage > 60" | bc -l) )); then
		colour="$(tput setaf 12)"
	else
		colour="$(tput setaf 1)"
	fi
	printf "%s% 7.2f%%%s: %s\n" "$colour" "$coverage" "$(tput sgr0)" "${f/$root\//}"
done

cd "$orig_pwd"
