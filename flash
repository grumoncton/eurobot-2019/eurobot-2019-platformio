#!/bin/env sh

log () {
  tput setaf 2; echo -e "\n$1"; tput sgr0
}


environment=$1
host=$2
local_firmware="./.pioenvs/$environment/firmware.bin"
remote_firmware="/tmp/firmware/$environment.bin"
python_root="../python"

if [ $host = "localhost" ]; then
  alias run=eval
else
  alias run="ssh $host"
fi

log "Compiling..."
pio run -e $environment

if [ $host != "localhost" ]; then
  log "Transfering..."
  run "mkdir -p `dirname $remote_firmware`"
  scp $local_firmware $host:$remote_firmware

  tput setaf 2
  echo -ne "\nChecking integrity... "

  local_checksum=`sha256sum $local_firmware | awk '{ print $1 }'`
  remote_checksum=`run "sha256sum $remote_firmware" | awk '{ print $1 }'`

  if [ "$local_checksum" == "$remote_checksum" ]; then
    echo "passed!"
    tput sgr0
  else
    tput setaf 1
    echo "Checksums differ on local and remote firmware. Exiting."
    tput sgr0
    exit 1
  fi
fi

log "Putting target in bootloader..."
device=`run "$python_root/cli.py" get_port $environment || exit 1`
run "$python_root/cli.py" send_command $device enter_bootloader || exit 1

log "Flashing target..."
while true; do
  run stm32flash -v -w "./.pioenvs/$environment/firmware.bin" -g 0x0 $device -b 115200 2>&1 \
    | tee /tmp/stm32flash.log

  grep -q "Failed to init device." /tmp/stm32flash.log
  [ $? -gt 0 ] && break

  tput setaf 1
  echo "Failed to initialize device. Trying again."
  tput sgr0
done
