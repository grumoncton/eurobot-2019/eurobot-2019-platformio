#ifndef STM32_CONSTANTS_H
#define STM32_CONSTANTS_H

#define __IO volatile

#define PF_0 (0)
#define PF_1 (0)
#define PF_2 (0)
#define PF_3 (0)
#define PF_4 (0)
#define PF_5 (0)
#define PF_6 (0)
#define PF_7 (0)
#define PF_8 (0)
#define PF_9 (0)
#define PF_10 (0)
#define PF_11 (0)
#define PF_12 (0)
#define PF_13 (0)
#define PF_14 (0)
#define PF_15 (0)

#define PA_0 (0)
#define PA_1 (0)
#define PA_2 (0)
#define PA_3 (0)
#define PA_4 (0)
#define PA_5 (0)
#define PA_6 (0)
#define PA_7 (0)
#define PA_8 (0)
#define PA_9 (0)
#define PA_10 (0)
#define PA_11 (0)
#define PA_12 (0)
#define PA_13 (0)
#define PA_14 (0)
#define PA_15 (0)

#define USBTX (0)
#define USBRX (0)

void HAL_RCC_DeInit();
void HAL_DeInit();

void __HAL_RCC_SYSCFG_CLK_ENABLE();
void __HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();
void __set_MSP(int address);

struct _SysTick {
	int *CTRL;
	int *LOAD;
	int *VAL;
};

extern _SysTick *SysTick;

#endif
