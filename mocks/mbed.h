#ifndef MBED_H
#define MBED_H

#include <stdio.h>
#include <cstring>
#include <math.h>
#include <stm32f3xx.h>
// #include <fakeit.hpp>

#define __IO volatile

typedef int PinName;

// using namespace fakeit;

class Timer {
	public:
		virtual void start(){}
		virtual void stop(){}
		virtual void reset(){}
		virtual float read(){
			return 0.0;
		}
};
class Ticker {
	public:
		void attach(void (*func)(), float t){}
};

class LowPowerTimer: public Timer {};
// extern Mock<LowPowerTimer> MockedLowPowerTimer;

class LowPowerTicker: public Ticker {};

class Serial {
	public:
		Serial(int tx, int rx, char *name=NULL, int baud=9600) {}
		virtual void attach(void (*func)()){}
		virtual void printf(const char *format, ...){}
		virtual char getc();

		const char *_getcReturnValues;
		uint16_t _getcCharacterPointer = 0;
		void _getcSetReturnValues(const char *message);

		void _mockClear();
};

void wait(int s);

class I2CSlave {
	public:
		I2CSlave(int SDA, int SCL) {}
		bool write(char *message, uint8_t length);
		bool read(char *message, uint8_t length);
		char read();
		void address(uint8_t address);
		uint8_t receive();
		static const uint8_t WriteAddressed = 0;
		static const uint8_t ReadAddressed = 1;
};

class DigitalIn {
	public:
		DigitalIn(PinName pin) {}
		bool operator!() { return false; }
};

class DigitalOut {
	public:
		DigitalOut(PinName pin) {}
		bool operator!() { return false; }
};

void wait_us(int);
#endif
