#ifndef SERVO_H
#define SERVO_H

class Servo {
	public:
		Servo(int pin);
		Servo& operator= (float percent);

		void write(float percent);
		float read();
		void position(float degrees);
		void calibrate(float range, float degrees);
};

#endif
