#include "mbed.h"
#include "comms-constants.h"

void wait(int s){}

Mock<Timer> MockedTimer;
Mock<LowPowerTimer> MockedLowPowerTimer;

char Serial::getc() {
	return this->_getcReturnValues[this->_getcCharacterPointer++];
}
void Serial::_getcSetReturnValues(const char *message) {
	this->_getcReturnValues = message;
	this->_getcCharacterPointer = 0;
}

void Serial::_mockClear() {
	this->_getcCharacterPointer = 0;
}
// Mock<Serial> MockedSerial;
