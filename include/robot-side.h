#ifndef ROBOT_SIDE_H
#define ROBOT_SIDE_H

enum class RobotSide : int8_t {
	RIGHT = 1,
	LEFT = -1,
};

#endif
