#include "tilter.h"

const uint16_t tilterAnglesUp[] { 1280, 1500 };
const uint16_t tilterAnglesDown[] { 710, 2050 };

AsyncServo tilters[] { D6, D7 };
Stepper stepper(D11, D13, 2000, NC);

void moveTilter(TilterSide side) {
	stepper.setTarget(TILTER_TRAVEL, (bool) side, TILTER_DELAY, 0);
}

void tilterSortRight() {
	moveTilter(TilterSide::RIGHT);
}

void tilterSortLeft() {
	moveTilter(TilterSide::LEFT);
}

void initTilter() {
	for (uint8_t i = 0; i < sizeof(tilters) / sizeof(AsyncServo); i++) {
		tilters[i].Enable(tilterAnglesUp[i], 20000);
	}
}

void raiseTilter() {
	for (uint8_t i = 0; i < sizeof(tilters) / sizeof(AsyncServo); i++) {
		tilters[i].startSlowRotation(tilterAnglesUp[i], 60);
	}
}

void lowerTilter() {
	for (uint8_t i = 0; i < sizeof(tilters) / sizeof(AsyncServo); i++) {
		tilters[i].SetPosition(tilterAnglesDown[i]);
	}
}
