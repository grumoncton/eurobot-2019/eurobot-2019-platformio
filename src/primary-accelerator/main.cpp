#include "main.h"

int main() {
	commsInit(ADDRESS);
	initEnvInstructions();
	initTilter();
	lowerTilter();

	comms.status = CommsStatus::READY;

	while (true) {
		commsLoop();
	}
}
