#include "env-instruction-handlers.h"

void handleRaiseTilter() {
	raiseTilter();
	commsAck();
}

void handleLowerTilter() {
	lowerTilter();
	commsAck();
}

void handleTilterSortLeft() {
	tilterSortLeft();
	commsAck();
}

void handleTilterSortRight() {
	tilterSortRight();
	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::TILTER_RAISE, handleRaiseTilter);
	addInstructionHandler((char) EnvInstruction::TILTER_LOWER, handleLowerTilter);

	addInstructionHandler((char) EnvInstruction::STEPPER_SORT_LEFT, handleTilterSortLeft);
	addInstructionHandler((char) EnvInstruction::STEPPER_SORT_RIGHT, handleTilterSortRight);
}
