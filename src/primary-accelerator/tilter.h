#ifndef TILTER_H
#define TILTER_H

#include <mbed.h>
#include <async-servo.h>
#include <Stepper.h>

const uint32_t TILTER = 100;
const uint32_t TILTER_DELAY = 30000;
const uint32_t TILTER_TRAVEL = 30;

enum class TilterSide { RIGHT = 0, LEFT = 1 };

void moveTilter(TilterSide side);
void tilterSortLeft();
void tilterSortRight();
void initTilter();
void raiseTilter();
void lowerTilter();

#endif
