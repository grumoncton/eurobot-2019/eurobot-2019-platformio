#ifndef LEADSCREW_H
#define LEADSCREW_H

#include <mbed.h>
#include <Stepper.h>

const uint16_t LEADSCREW_HOMING_SPEED = 700;
const uint16_t LEADSCREW_SPEED = 500;
const bool LEADSCREW_ACCEL = true;

enum class LeadscrewState {
	UNHOMED = -1,
	EXTENDED = 1,
	RETRACTED = 0,
	HOMING = 2,
};

extern LeadscrewState leadscrewState;
extern Stepper leadscrewStepper;

enum class LeadscrewDirection {
	DOWN = true,
	UP = false,
};

const uint16_t STEPS_TRAVEL = 1800;

const PinName LEADSCREW_LIMIT_SWITCH_PIN = D10;

extern DigitalIn leadscrewLimitSwitch;

uint8_t leadscrewExtend();
uint8_t leadscrewRetract();
void leadscrewStop();
void homeLeadscrew();
void homeLeadscrewInGame();

#endif
