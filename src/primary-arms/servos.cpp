#include "servos.h"

uint16_t mainArmAnglesDown[] { 1180, 2130 };
uint16_t mainArmAnglesUp[] { 2000, 1200 };
uint16_t mainArmSemiAngles[] { 1550, 1720 };
HardServo mainArmServos[] { D12, D11 };
// Right: S6 -> D11
// Left: S7 -> D12

uint16_t goldArmAnglesDown[] { 1275, 1720 };
uint16_t goldArmAnglesUp[] { 1700, 1275 };
Servo goldArmServos[] { D7, D8 };

/**
 * Raise main arm
 *
 * Set both servos on the main arm to their raised position.
 */

void raiseMainArm() {
	for (uint8_t i = 0; i < sizeof(mainArmServos) / sizeof(HardServo); i++) {
		mainArmServos[i].SetPosition(mainArmAnglesUp[i]);
	}
}


/**
 * Lower main arm
 *
 * Set both servos on the main arm to their lowered position.
 */

void lowerMainArm() {
	for (uint8_t i = 0; i < sizeof(mainArmServos) / sizeof(HardServo); i++) {
		mainArmServos[i].SetPosition(mainArmAnglesDown[i]);
	}
}


void semiMainArm() {
	for (uint8_t i = 0; i < sizeof(mainArmServos) / sizeof(HardServo); i++) {
		mainArmServos[i].SetPosition(mainArmSemiAngles[i]);
	}
}


void initGoldArm() {
	for (uint8_t i = 0; i < sizeof(goldArmServos) / sizeof(Servo); i++) {
		goldArmServos[i].Enable(goldArmAnglesUp[i], 20e3);
	}
}


/**
 * Raise gold arm
 *
 * Raise specified servo on gold arm.
 */

void raiseGoldArm(uint8_t servoIndex) {
	goldArmServos[servoIndex].SetPosition(goldArmAnglesUp[servoIndex]);
}


/**
 * Lower gold arm
 *
 * Lower specified servo on gold arm.
 */

void lowerGoldArm(uint8_t servoIndex) {
	goldArmServos[servoIndex].SetPosition(goldArmAnglesDown[servoIndex]);
}
