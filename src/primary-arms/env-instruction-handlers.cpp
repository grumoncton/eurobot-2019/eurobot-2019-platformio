#include "env-instruction-handlers.h"

void handleLeadscrewExtend() {
	leadscrewExtend();
	commsAck();
}

void handleLeadscrewRetract() {
	leadscrewRetract();
	commsAck();
}

void handleHomeLeadscrewInGame() {
	homeLeadscrewInGame();
	commsAck();
}


/**
 * Set suckers
 *
 * Get state from first arg. Each relay is given a bit in this byte.
 */

void handleSetSuckers() {
	uint8_t state = comms.args[0];

	setSuckers(state);
	commsAck();
}


void handleActivateSolenoids() {
	activateSolenoids();
	commsAck();
}

void handleDeactivateSolenoids() {
	deactivateSolenoids();
	commsAck();
}


void handleRaiseMainArm() {
	raiseMainArm();
	commsAck();
}

void handleLowerMainArm() {
	lowerMainArm();
	commsAck();
}

void handleSemiMainArm() {
	semiMainArm();
	commsAck();
}

/**
 * Raise gold arm.
 *
 * Get index (which arm) from first arg.
 */

void handleRaiseGoldArm() {
	uint8_t servoIndex = comms.args[0];
	raiseGoldArm(servoIndex);
	commsAck();
}


/**
 * Lower gold arm.
 *
 * Get index (which arm) from first arg.
 */

void handleLowerGoldArm() {
	uint8_t servoIndex = comms.args[0];
	lowerGoldArm(servoIndex);
	commsAck();
}


/**
 * Trigger Experience by sending rf signal
 */

void handleTriggerExperience() {
	TriggerExperience();
	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::LEADSCREW_EXTEND, handleLeadscrewExtend);
	addInstructionHandler((char) EnvInstruction::LEADSCREW_RETRACT, handleLeadscrewRetract);
	addInstructionHandler((char) EnvInstruction::LEADSCREW_HOME_IN_GAME, handleHomeLeadscrewInGame);

	addInstructionHandler((char) EnvInstruction::SET_SUCKERS, handleSetSuckers);
	addInstructionHandler((char) EnvInstruction::ACTIVATE_SOLENOIDS, handleActivateSolenoids);
	addInstructionHandler((char) EnvInstruction::DEACTIVATE_SOLENOIDS, handleDeactivateSolenoids);

	addInstructionHandler((char) EnvInstruction::RAISE_MAIN_ARM, handleRaiseMainArm);
	addInstructionHandler((char) EnvInstruction::LOWER_MAIN_ARM, handleLowerMainArm);
	addInstructionHandler((char) EnvInstruction::SEMI_MAIN_ARM, handleSemiMainArm);
	addInstructionHandler((char) EnvInstruction::RAISE_GOLD_ARM, handleRaiseGoldArm);
	addInstructionHandler((char) EnvInstruction::LOWER_GOLD_ARM, handleLowerGoldArm);

	addInstructionHandler((char) EnvInstruction::TRIGGER_EXPERIENCE, handleTriggerExperience);
}
