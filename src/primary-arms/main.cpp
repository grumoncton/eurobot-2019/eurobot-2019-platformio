#include "main.h"

int main() {
	commsInit(ADDRESS);
	initEnvInstructions();
	initGoldArm();
	raiseMainArm();

	leadscrewLimitSwitch.mode(PullUp);
	homeLeadscrew();

	comms.status == CommsStatus::READY;

	while (true) {
		commsLoop();

		if (leadscrewState == LeadscrewState::HOMING && leadscrewLimitSwitch) {
			leadscrewStop();
			leadscrewState = LeadscrewState::RETRACTED;
		}

		comms.status = leadscrewStepper.done ? CommsStatus::READY : CommsStatus::WORKING;
	}
}
