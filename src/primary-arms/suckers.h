#ifndef SUCKERS_H
#define SUCKERS_H

#include <mbed.h>

void setSuckers(uint8_t state);
void activateSolenoids();
void deactivateSolenoids();

#endif
