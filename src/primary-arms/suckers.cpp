#include "suckers.h"

DigitalOut suckers[] { A0, A1, A2 };

DigitalOut solenoids[] { A3, A6 };

void setSuckers(uint8_t state) {
	for (uint8_t i = 0; i < sizeof(suckers) / sizeof(DigitalOut); i++) {
		suckers[i] = state & (1 << i);
	}
}

void activateSolenoids() {
	for (uint8_t i = 0; i < sizeof(solenoids) / sizeof(DigitalOut); i++) {
		solenoids[i] = true;
	}
}

void deactivateSolenoids() {
	for (uint8_t i = 0; i < sizeof(solenoids) / sizeof(DigitalOut); i++) {
		solenoids[i] = false;
	}
}
