#ifndef SERVOS_H
#define SERVOS_H

#include <mbed.h>
#include <Servo.h>
#include <HardServo.h>

void raiseMainArm();
void lowerMainArm();
void initGoldArm();
void semiMainArm();
void raiseGoldArm(uint8_t servoIndex);
void lowerGoldArm(uint8_t servoIndex);
#endif
