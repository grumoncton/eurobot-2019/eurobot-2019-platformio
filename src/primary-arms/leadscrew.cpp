#include "leadscrew.h"

Stepper leadscrewStepper(D2, D3, 300, A7);

LeadscrewState leadscrewState = LeadscrewState::UNHOMED;

DigitalIn leadscrewLimitSwitch(LEADSCREW_LIMIT_SWITCH_PIN);

uint8_t leadscrewExtend() {
	if (leadscrewState == LeadscrewState::EXTENDED) {
		return 1;
	}

	leadscrewStepper.setTarget(
		STEPS_TRAVEL,
		(bool) LeadscrewDirection::UP,
		LEADSCREW_SPEED,
		LEADSCREW_ACCEL);

	leadscrewState = LeadscrewState::EXTENDED;

	return 0;
}

uint8_t leadscrewRetract() {
	if (leadscrewState == LeadscrewState::RETRACTED) {
		return 1;
	}

	leadscrewStepper.setTarget(
		STEPS_TRAVEL,
		(bool) LeadscrewDirection::DOWN,
		LEADSCREW_SPEED,
		LEADSCREW_ACCEL);

	leadscrewState = LeadscrewState::RETRACTED;

	return 0;
}

void homeLeadscrew() {
	leadscrewStepper.startRotation(
		(bool) LeadscrewDirection::DOWN,
		LEADSCREW_HOMING_SPEED,
		LEADSCREW_ACCEL);

	while (!leadscrewLimitSwitch);

	leadscrewStepper.reset();

	leadscrewState = LeadscrewState::RETRACTED;
}

void homeLeadscrewInGame() {
	leadscrewState = LeadscrewState::HOMING;

	leadscrewStepper.startRotation(
		(bool) LeadscrewDirection::DOWN,
		LEADSCREW_HOMING_SPEED,
		LEADSCREW_ACCEL);
}

void leadscrewStop() {
	leadscrewStepper.reset();
}
