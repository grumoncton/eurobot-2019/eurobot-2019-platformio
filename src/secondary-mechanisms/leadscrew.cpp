#include "leadscrew.h"

DigitalIn limitSwitch(LEADSCREW_LIMIT_SWITCH_PIN);

Stepper stepper(
		LEADSCREW_CLK_PIN,
		LEADSCREW_DIR_PIN,
		LEADSCREW_START_STOP_DELAY,
		LEADSCREW_EN_PIN);

LeadscrewState currentState = LeadscrewState::UNHOMED;

void initLeadscrew() {
	limitSwitch.mode(PullUp);
}

void homeLeadscrew() {
	stepper.startRotation(
		(bool) LeadscrewDirection::DOWN,
		LEADSCREW_HOMING_SPEED,
		LEADSCREW_ACCEL);

	while (!limitSwitch);

	stepper.reset();

	currentState = LeadscrewState::HOME;
}

uint8_t leadscrewMoveTo(LeadscrewState state) {
	if (currentState == LeadscrewState::UNHOMED) {
		return 1;
	}

	uint16_t height = LEADSCREW_HEIGHTS[(uint8_t) state];

	int16_t delta = height - LEADSCREW_HEIGHTS[(uint8_t) currentState];

#ifdef DEBUG
	printf("Target height: %d current height %d delta %d\n", height, LEADSCREW_HEIGHTS[(uint8_t) currentState], delta);
#endif

	stepper.setTarget(
		abs(delta),
		delta < 0,
		LEADSCREW_SPEED,
		LEADSCREW_ACCEL);

	currentState = state;

	return 0;
}
