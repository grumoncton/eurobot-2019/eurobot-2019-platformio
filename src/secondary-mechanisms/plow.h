#ifndef PLOW_H
#define PLOW_H

#include <mbed.h>
#include <Servo.h>

void initPlow();
void openPlow();
void closePlow();
void cradlePlow();
void splitPlow();
void straightPlow();

#endif
