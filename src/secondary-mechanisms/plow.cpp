#include "plow.h"


Timeout rightTimeout;
Timeout leftTimeout;


const uint16_t MID_ANGLE = 1500;
const uint16_t PWM_PERIOD = 20e3;

const uint16_t LEFT_OPEN_PWM = 2150;
const uint16_t RIGHT_OPEN_PWM = 2400;

const uint16_t LEFT_CLOSED_PWM = 500;
const uint16_t RIGHT_CLOSED_PWM = 500;

Servo left(D6);
Servo right(D8);

void initPlow() {
	left.Enable(1100, PWM_PERIOD);
	right.Enable(MID_ANGLE, PWM_PERIOD);
}

void openPlow() {
	left.Enable(1100, PWM_PERIOD);
	left.SetPosition(LEFT_OPEN_PWM);
	right.Enable(MID_ANGLE, PWM_PERIOD);
	right.SetPosition(RIGHT_OPEN_PWM);
}

void closePlow() {
	left.Enable(1100, PWM_PERIOD);
	left.SetPosition(LEFT_CLOSED_PWM);
	leftTimeout.attach(callback(&left, &Servo::Disable), 0.5);

	right.Enable(MID_ANGLE, PWM_PERIOD);
	right.SetPosition(RIGHT_CLOSED_PWM);
	rightTimeout.attach(callback(&right, &Servo::Disable), 0.5);
}

void cradlePlow() {
	left.Enable(1100, PWM_PERIOD);
	left.SetPosition(MID_ANGLE);
	right.Enable(MID_ANGLE, PWM_PERIOD);
	right.SetPosition(MID_ANGLE);
}

void splitPlow() {
	left.Enable(1100, PWM_PERIOD);
	left.SetPosition(1200);
	right.Enable(MID_ANGLE, PWM_PERIOD);
	right.SetPosition(1200);
}

void straightPlow() {
	left.Enable(1100, PWM_PERIOD);
	left.SetPosition(1100);
	right.Enable(MID_ANGLE, PWM_PERIOD);
	right.SetPosition(MID_ANGLE);
}
