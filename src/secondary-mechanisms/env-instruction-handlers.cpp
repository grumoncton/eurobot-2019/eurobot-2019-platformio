#include "env-instruction-handlers.h"

void scaraHandleMoveTo() {
	float x, y;

	memcpy(&x, comms.args, sizeof(float));
	memcpy(&y, comms.args + sizeof(float), sizeof(float));

	scaraMoveTo(x, y);

	commsAck();
}

void scaraHandleStash() {
	RobotSide side = (RobotSide)(comms.args[0]);
	scaraStash(side);

	commsAck();
}

void handleLeadscrewMoveTo() {
	LeadscrewState state = (LeadscrewState) comms.args[0];

	leadscrewMoveTo(state);

	commsAck();
}

void handleEnableVacuum() {
	enableVacuum();
	commsAck();
}

void handleDisableVacuum() {
	disableVacuum();
	commsAck();
}

void handleOpenPlow() {
	openPlow();
	commsAck();
}

void handleClosePlow() {
	closePlow();
	commsAck();
}

void handleCradlePlow() {
	cradlePlow();
	commsAck();
}

void handleSplitPlow() {
	splitPlow();
	commsAck();
}

void handleStraightPlow() {
	straightPlow();
	commsAck();
}

void scaraHandleSpoon() {
	RobotSide side = (RobotSide) comms.args[0];
	scaraSpoon(side);

	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::SCARA_MOVE_TO, scaraHandleMoveTo);
	addInstructionHandler((char) EnvInstruction::SCARA_STASH, scaraHandleStash);
	addInstructionHandler((char) EnvInstruction::SCARA_SPOON, scaraHandleSpoon);
	addInstructionHandler((char) EnvInstruction::LEADSCREW_MOVE_TO, handleLeadscrewMoveTo);

	addInstructionHandler((char) EnvInstruction::ENABLE_VACUUM, handleEnableVacuum);
	addInstructionHandler((char) EnvInstruction::DISABLE_VACUUM, handleDisableVacuum);

	addInstructionHandler((char) EnvInstruction::OPEN_PLOW, handleOpenPlow);
	addInstructionHandler((char) EnvInstruction::CLOSE_PLOW, handleClosePlow);
	addInstructionHandler((char) EnvInstruction::CRADLE_PLOW, handleCradlePlow);
	addInstructionHandler((char) EnvInstruction::SPLIT_PLOW, handleSplitPlow);
	addInstructionHandler((char) EnvInstruction::STRAIGHT_PLOW, handleStraightPlow);
}
