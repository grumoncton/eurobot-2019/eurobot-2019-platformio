#ifndef LEADSCREW_H
#define LEADSCREW_H

#include <mbed.h>
#include <Stepper.h>
const PinName LEADSCREW_CLK_PIN = D11;
const PinName LEADSCREW_DIR_PIN = D13;
const PinName LEADSCREW_EN_PIN = D12;

const uint16_t LEADSCREW_START_STOP_DELAY = 400;
// Speed limit at 1A current limit
// const uint16_t LEADSCREW_SPEED = 59;

const uint16_t LEADSCREW_SPEED = 200;
const uint16_t LEADSCREW_HOMING_SPEED = 400;

const bool LEADSCREW_ACCEL = true;

enum class LeadscrewDirection { DOWN = true, UP = false };

enum class LeadscrewState {
	UNHOMED = -1,
	HOME = 0,
	PUCK_HEIGHT = 1,
	STASH_HEIGHT = 2,
	CLEAR_STASH_HEIGHT = 3,
	ELBOW_PUCK_HEIGHT = 4,
};

const uint16_t LEADSCREW_HEIGHTS[] = {
	0,
	500,
	6800,
	7600,
	5000,
};

const PinName LEADSCREW_LIMIT_SWITCH_PIN = D10;

void homeLeadscrew();
void initLeadscrew();
uint8_t leadscrewMoveTo(LeadscrewState state);

extern Stepper stepper;

#endif
