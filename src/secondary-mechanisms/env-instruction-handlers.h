#ifndef ENV_INSTRUCTION_HANDLERS
#define ENV_INSTRUCTION_HANDLERS

#include "comms-constants.h"
#include "base-instruction-handlers.h"
#include "env-instructions.h"
#include "scara.h"
#include "leadscrew.h"
#include "vacuum.h"
#include "plow.h"

void scaraHandleMoveTo();
void handleLeadscrewMoveTo();

void initEnvInstructions();

#endif
