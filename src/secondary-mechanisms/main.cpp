#include "main.h"

int main() {
	commsInit(ADDRESS);
	initEnvInstructions();
	initPlow();
	initScara();
	initLeadscrew();

	homeLeadscrew();

	leadscrewMoveTo(LeadscrewState::CLEAR_STASH_HEIGHT);

	closePlow();

	comms.status = CommsStatus::READY;

	while (true) {
		commsLoop();

		comms.status = stepper.done ? CommsStatus::READY : CommsStatus::WORKING;
	}
}
