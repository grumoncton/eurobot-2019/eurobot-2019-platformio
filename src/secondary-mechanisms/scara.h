#ifndef SCARA_H
#define SCARA_H

#include <mbed.h>
#include <Servo.h>
#include "robot-side.h"

void initScara();
uint8_t scaraMoveTo(float x, float y);
void scaraSpoon(RobotSide side);
void scaraStash(RobotSide side);

#endif
