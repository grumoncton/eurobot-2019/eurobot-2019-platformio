#ifndef VACUUM_H
#define VACUUM_H

#include "mbed.h"

void enableVacuum();
void disableVacuum();

#endif
