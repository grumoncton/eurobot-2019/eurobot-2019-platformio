#include "vacuum.h"


DigitalOut relay(A2);

void enableVacuum() {
	relay = 1;
}

void disableVacuum() {
	relay = 0;
}
