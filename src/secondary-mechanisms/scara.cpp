#include "scara.h"

const uint16_t MID_PWM = 1500;
const uint16_t PWM_PERIOD = 8e3;

// Suction cup diameter
const uint8_t SUCT_DIA = 25;
// Puck diameter
const uint8_t PUCK_DIA = 78;
// Max distance off of puck's center that suction cup can still pick it up
const float OFFSET = (PUCK_DIA - SUCT_DIA) / 2;

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define constrain(a, x, b) (min(b, max(a, x)))
#define square(a) (a * a)
#define deg(a) (a * M_PI / 180)

// Calculates angle opposite to side c
float solveCosineLawAngle(float a, float b, float c) {
	return acos((square(a) + square(b) - square(c)) / (2 * a * b));
}

float solveCosineLawSide(float A, float B, float c) {
	return sqrt(square(A) + square(B) - 2 * A * B * cos(c));
}

// Calculate angle opposite to B
float solveSineLawAngle(float a, float A, float B) {
	return asin(sin(a) * B / A);
}

struct Segment {
	const uint8_t pin;
	const uint8_t length;
	const float maxAngle;

	const uint16_t pwmForMinAngle;
	const uint16_t pwmForMaxAngle;

	Servo servo;
	Segment(
			const uint8_t _pin,
			const uint8_t _length,
			const float _max,
			const uint16_t _pwmForMinAngle,
			const uint16_t _pwmForMaxAngle
	) :
		pin { _pin },
		length { _length },
		maxAngle { _max },
		pwmForMinAngle { _pwmForMinAngle },
		pwmForMaxAngle { _pwmForMaxAngle },
		servo((PinName) pin)
	{ }
};

Segment shoulder = {
	.pin = D9,
	.length = 80,
	.maxAngle = deg(90),
	.pwmForMinAngle = 790,
	.pwmForMaxAngle = 2100,
};

Segment elbow = {
	.pin = D7,
	.length = 80,
	.maxAngle = deg(135),
	.pwmForMinAngle = 580,
	.pwmForMaxAngle = 2520,
};

const float MAX_REACH = shoulder.length + elbow.length;
const float MIN_REACH = solveCosineLawSide(
	shoulder.length,
	elbow.length,
	M_PI - elbow.maxAngle);

uint8_t calculateScaraAnglesSingleQuadrant(
	float _x,
	float _y,
	float &_shoulder_a,
	float &_elbow_a
) {

	// Calculate distance from origin to position (x,y)
	float d = sqrt(square(_x) + square(_y));

	// Verify that arm can reach puck
	if (d < MIN_REACH - OFFSET || d > MAX_REACH + OFFSET) {
#ifdef DEBUG
		printf("Arm cannot reach %lf (min: %lf, max: %lf)\n", d, MIN_REACH, MAX_REACH);
#endif
		return 1;
	}

	// If puck's center is out of range, but within offset,
	// set d to reachable position nearest to center
	d = constrain(MIN_REACH, d, MAX_REACH);

	// Calculate elbow angle
	// We know the three sides of the triangle so we can calculate with cosine law
	float elbow_a_inside = solveCosineLawAngle(shoulder.length, elbow.length, d);

	// Since the servo angle is relative to straight, subtract 180deg
	float elbow_a = M_PI - elbow_a_inside;

	// Calculate angle from y-axis to hypotenuse of x and y
	float hypotenuse_a = _x == 0 ? 0 : atan2(_x, _y);

	// Calculate angle from shoulder segment to hypotenuse of x and y
	// This is the angle inside the triangle
	float shoulderToHypotenuse_a = solveSineLawAngle(elbow_a_inside, d, elbow.length);

	// The target shoulder angle is the difference between these last two
	float shoulder_a = hypotenuse_a - shoulderToHypotenuse_a;

#ifdef DEBUG
	// ser.printf("elbow inside: %lf, elbow: %lf, shoulder: %lf\n", elbow_a_inside, elbow_a, shoulder_a);
#endif

	// Ensure angles reachable
	if (shoulder_a > shoulder.maxAngle) {
#ifdef DEBUG
		printf(
			"Shoulder cannot reach required angle %lf\n (max: %lf)",
			shoulder_a,
			shoulder.maxAngle);
#endif
		return 1;
	}
	if (elbow_a > elbow.maxAngle) {
#ifdef DEBUG
		printf(
			"Elbow cannot reach required angle %lf\n (max: %lf)",
			elbow_a,
			elbow.maxAngle);
#endif
		return 1;
	}

	// Set references and exit successfully
	_elbow_a = elbow_a;
	_shoulder_a = shoulder_a;

	return 0;
}

uint8_t calculateScaraAngles(float _x, float _y, float &_shoulder_a, float &_elbow_a) {
	uint8_t returnCode = calculateScaraAnglesSingleQuadrant(fabs(_x), _y, _shoulder_a, _elbow_a);

	if (returnCode != 0) return returnCode;

	if (_x < 0) {
		_shoulder_a = - _shoulder_a;
		_elbow_a = - _elbow_a;
	}

	return 0;
}

uint16_t pwmForAngle(float angle, Segment &segment) {
	return angle * (segment.pwmForMaxAngle - segment.pwmForMinAngle) / (2 * segment.maxAngle) +
		(segment.pwmForMaxAngle + segment.pwmForMinAngle) / 2;
}

uint8_t scaraMoveTo(float x, float y) {
	float shoulder_a, elbow_a;
	uint8_t returnCode = calculateScaraAngles(x, y, shoulder_a, elbow_a);

	if (returnCode != 0) return returnCode;

#ifdef DEBUG
	// ser.printf("shoulder: %lf, elbow: %lf\n", shoulder_a, elbow_a);
#endif

	shoulder.servo.SetPosition(pwmForAngle(shoulder_a, shoulder));
	elbow.servo.SetPosition(pwmForAngle(elbow_a, elbow));

	return 0;
}

void initScara() {
	shoulder.servo.Enable(MID_PWM, PWM_PERIOD);
	elbow.servo.Enable(MID_PWM, PWM_PERIOD);
}

void scaraStash(RobotSide side) {
	scaraMoveTo(80 * (int8_t) side, 10);
}

void scaraSpoon(RobotSide side) {
	scaraMoveTo((int8_t) side * 145, 40);
}
