#include "env-instruction-handlers.h"

void handleSetSorters() {
	uint8_t state = comms.args[0];

	setSorters(state);
	commsAck();
}

void handleRaiseFlipper() {
	RobotSide robotSide = (RobotSide) comms.args[0];

	raiseFlipper(robotSide);
	commsAck();
}

void handleMidFlipper() {
	RobotSide robotSide = (RobotSide) comms.args[0];

	midFlipper(robotSide);
	commsAck();
}


void handleLowerFlipper() {
	RobotSide robotSide = (RobotSide) comms.args[0];

	lowerFlipper(robotSide);
	commsAck();
}

void handleRaiseSlideBlocker() {
	RobotSide robotSide = (RobotSide) comms.args[0];

	raiseSlideBlocker(robotSide);
	commsAck();
}

void handleLowerSlideBlocker() {
	RobotSide robotSide = (RobotSide) comms.args[0];

	lowerSlideBlocker(robotSide);
	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::SET_SORTERS, handleSetSorters);

	addInstructionHandler((char) EnvInstruction::RAISE_FLIPPER, handleRaiseFlipper);
	addInstructionHandler((char) EnvInstruction::LOWER_FLIPPER, handleLowerFlipper);
	addInstructionHandler((char) EnvInstruction::RAISE_SLIDE_BLOCKER, handleRaiseSlideBlocker);
	addInstructionHandler((char) EnvInstruction::LOWER_SLIDE_BLOCKER, handleLowerSlideBlocker);
	addInstructionHandler((char) EnvInstruction::MID_FLIPPER, handleMidFlipper);
}
