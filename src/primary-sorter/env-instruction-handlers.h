#ifndef ENV_INSTRUCTION_HANDLERS
#define ENV_INSTRUCTION_HANDLERS

#include "comms.h"
#include "comms-constants.h"
#include "servos.h"
#include "env-instructions.h"
#include "robot-side.h"

void initEnvInstructions();

#endif
