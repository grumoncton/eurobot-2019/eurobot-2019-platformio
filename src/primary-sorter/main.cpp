#include "main.h"

int main() {
	commsInit(ADDRESS);
	initEnvInstructions();
	initServos();

	comms.status = CommsStatus::READY;

	while (true) {
		commsLoop();
	}
}
