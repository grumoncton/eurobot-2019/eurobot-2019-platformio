#ifndef SORTERS_H
#define SORTERS_H

#include <mbed.h>
#include <Servo.h>
#include "robot-side.h"

void setSorters(uint8_t state);

void initServos();
void raiseFlipper(RobotSide robotSide);
void midFlipper(RobotSide robotSide);
void lowerFlipper(RobotSide robotSide);
void raiseSlideBlocker(RobotSide robotSide);
void lowerSlideBlocker(RobotSide robotSide);

#endif
