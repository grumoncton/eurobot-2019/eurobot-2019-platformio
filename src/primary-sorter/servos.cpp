#include "servos.h"

const uint16_t sorterAngleUp[] = { 1930, 1180, 1080, 1100, 1860, 1880 };
const uint16_t sorterAngleDown[] = { 1120, 1950, 1890, 1880, 1140, 1120 };
Servo sorters[] { D2, D3, D6, D7, D8, D9 };

// Puck flippers for slide
const uint16_t flipperAngleUp[] = { 1930, 2100 };
const uint16_t flipperAngleMid[] = { 1220, 1760 };
const uint16_t flipperAngleDown[] = { 1500, 1500 };
Servo flippers[] = { D10, D11 };

// Arms that control flow of pucks in slide
const uint16_t slideBlockerAngleUp[] = { 2200, 800 };
const uint16_t slideBlockerAngleDown[] = { 1500, 1500 };
Servo slideBlockers[] = { D12, A3 };


void initServos() {
	for (uint8_t i = 0; i < sizeof(sorters) / sizeof(Servo); i++) {
		sorters[i].Enable(sorterAngleDown[i], 20000);
	}
	for (uint8_t i = 0; i < sizeof(flippers) / sizeof(Servo); i++) {
		flippers[i].Enable(flipperAngleDown[i], 20000);
	}
	for (uint8_t i = 0; i < sizeof(slideBlockers) / sizeof(Servo); i++) {
		slideBlockers[i].Enable(slideBlockerAngleDown[i], 20000);
	}
}

/**
 * Set sorters
 *
 * Set all sorters based on bits in byte given.
 */

void setSorters(uint8_t state) {
	for (uint8_t i = 0; i < sizeof(sorters) / sizeof(Servo); i++) {
		sorters[i].SetPosition(state & (1 << i)
			? sorterAngleUp[i]
			: sorterAngleDown[i]);
	}
}


/**
 * Raise flipper for table side
 */

void raiseFlipper(RobotSide robotSide) {
	switch (robotSide) {
		case RobotSide::LEFT:
			flippers[0].SetPosition(flipperAngleUp[0]);
			break;
		case RobotSide::RIGHT:
			flippers[1].SetPosition(flipperAngleUp[1]);
			break;
	}	
}

/**
 * Place flipper in mid position for table side
 */

void midFlipper(RobotSide robotSide) {
	switch (robotSide) {
		case RobotSide::LEFT:
			flippers[0].SetPosition(flipperAngleMid[0]);
			break;
		case RobotSide::RIGHT:
			flippers[1].SetPosition(flipperAngleMid[1]);
			break;
	}	
}

/**
 * Lower flipper for table side
 */

void lowerFlipper(RobotSide robotSide) {
	switch (robotSide) {
		case RobotSide::LEFT:
			flippers[0].SetPosition(flipperAngleDown[0]);
			break;
		case RobotSide::RIGHT:
			flippers[1].SetPosition(flipperAngleDown[1]);
			break;
	}	
}


/**
 * Raise side blocker for table side
 */

void raiseSlideBlocker(RobotSide robotSide) {
	switch (robotSide) {
		case RobotSide::LEFT:
			slideBlockers[0].SetPosition(slideBlockerAngleUp[0]);
			break;
		case RobotSide::RIGHT:
			slideBlockers[1].SetPosition(slideBlockerAngleUp[1]);
			break;
	}
}


/**
 * Lower side blocker for table side
 */

void lowerSlideBlocker(RobotSide robotSide) {
	switch (robotSide) {
		case RobotSide::LEFT:
			slideBlockers[0].SetPosition(slideBlockerAngleDown[0]);
			break;
		case RobotSide::RIGHT:
			slideBlockers[1].SetPosition(slideBlockerAngleDown[1]);
			break;
	}
}
