#include "env-instruction-handlers.h"


/**
 * Read current x,y,a position from encoders
 */

void handleGetPosition() {
	char response[sizeof(pos)];
	memcpy(&response, &pos, sizeof(pos));
	prepareFrame(response, sizeof(response));
}


/**
 * Update currently set position.
 *
 * This happens at the start of a match (constants like this should be stored
 * in the Python) and could come in handy if we ever update the position (from
 * the decawave, for example).
 */

void handleSetPosition() {
	memcpy(&pos, comms.args, sizeof(pos));

	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::GET_POSITION, handleGetPosition);
	addInstructionHandler((char) EnvInstruction::SET_POSITION, handleSetPosition);
}
