#include "main.h"

QEI encRight(ENC_RIGHT_A_PIN, ENC_RIGHT_B_PIN, ENC_RIGHT_X_PIN, PULSES_PER_REVOLUTION);
QEI encLeft(ENC_LEFT_A_PIN, ENC_LEFT_B_PIN, ENC_LEFT_X_PIN, PULSES_PER_REVOLUTION);

// TODO: Investigate low power ticker
Ticker ticker;


/*
 * Update position
 *
 * Use distance traveled in right and left encoder in order to approximate
 * change in x, y and heading. This function must be called in a timer at a
 * fixed interval
 */

void updatePosition() {

	// Change in right and left wheels
	double rightDelta = rightMultiplier * PULSES_TO_MM(encRight.getPulses());
	double leftDelta = leftMultiplier * PULSES_TO_MM(encLeft.getPulses());

	encRight.reset();
	encLeft.reset();

	// Distance traveled by center of robot
	double centerDelta = (rightDelta + leftDelta) / 2;

	// Change in heading (radians)
	double aDelta = (leftDelta - rightDelta) / WHEEL_SEPARATION;

	// Polar to rectangular
	double xDelta, yDelta;
	if (aDelta != 0) {
		xDelta = (centerDelta / aDelta) * (sin(pos.a + aDelta) - sin(pos.a));
		yDelta = (centerDelta / aDelta) * (cos(pos.a) - cos(pos.a + aDelta));
	} else {
		xDelta = centerDelta * cos(pos.a);
		yDelta = centerDelta * sin(pos.a);
	}

	// Update global position
	pos.x += xDelta;
	pos.y += yDelta;
	pos.a += aDelta;

	// Ensure angle between 0 and 2 pi
	pos.a = fmod(pos.a, 2 * M_PI);
}

#ifndef TEST
int main() {
	commsInit(ADDRESS);
	initEnvInstructions();

	// Update position in timer
	ticker.attach(&updatePosition, 10e-3);

	comms.status = CommsStatus::READY;

	while (true) {

#ifdef DEBUG
		 printf(
			 "x: %d y: %d a: %d\n",
			 (int) pos.x,
			 (int) pos.y,
			 (int) (pos.a * 180 / M_PI));

		 wait(0.5);
#endif

		commsLoop();
	}

	return 1;
}
#endif
