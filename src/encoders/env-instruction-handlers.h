#ifndef ENV_INSTRUCTION_HANDLERS
#define ENV_INSTRUCTION_HANDLERS

#include "comms-constants.h"
#include "base-instruction-handlers.h"
#include "env-instructions.h"
#include "position.h"

void initEnvInstructions();

#endif
