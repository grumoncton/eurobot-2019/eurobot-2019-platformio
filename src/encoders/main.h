#ifndef MAIN_H
#define MAIN_H

#include <stm32f3xx.h>
#include <mbed.h>
#include <QEI.h>
#include "comms.h"
#include "position.h"
#include "env-instruction-handlers.h"

#define ADDRESS (0x42)

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

const PinName ENC_RIGHT_X_PIN = NC;
const PinName ENC_RIGHT_A_PIN = PF_1;
const PinName ENC_RIGHT_B_PIN = PA_8;

const PinName ENC_LEFT_X_PIN = NC;
const PinName ENC_LEFT_A_PIN = PB_5;
const PinName ENC_LEFT_B_PIN = PA_4;

const double rightMultiplier = -1.0;
const double leftMultiplier = 1.0;

const uint16_t PULSES_PER_REVOLUTION = 1024;
const double WHEEL_DIAMETER = 50.0;
const double WHEEL_CIRCUMFERENCE = (M_PI * WHEEL_DIAMETER);
const double WHEEL_SEPARATION = 208.5;

#define PULSES_TO_MM(pulses) ((pulses) * (WHEEL_CIRCUMFERENCE) / (PULSES_PER_REVOLUTION))

#endif
