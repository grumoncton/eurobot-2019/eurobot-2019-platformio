#ifndef ENV_INSTRUCTION
#define ENV_INSTRUCTION

#include "base-instructions.h"

enum class EnvInstruction {
	GET_POSITION = 0x30,
	SET_POSITION = 0x31,
};

#endif
