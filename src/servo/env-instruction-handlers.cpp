#include "env-constants.h"
#include "env-instruction-handlers.h"

void writeAngle() {
	float target;
	memcpy(&target, &comms.args, sizeof(target));
	servo.position(target);

	commsAck();
}

void initEnvInstructions() {
	addInstructionHandler((char) EnvInstruction::MOVE_TO, writeAngle);
}
